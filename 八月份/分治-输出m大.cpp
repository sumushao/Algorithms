
#include<iostream>
 
using namespace std;
int a[100];
 
void swap(int &a, int &b)
{
	int temp;
	temp = b;
	b = a;
	a = temp;
}
void QuickSort(int a[], int s, int e, int m)
{
	if (s >= e)
		return;
	int k = a[s];
	int i = s, j = e;
	while (i != j)
	{
		if (j > i && a[j] > k)
			j--;
		swap(a[i], a[j]);
		if (j > i && a[i] < k)
			i++;
		swap(a[i], a[j]);
	}
 
	int b = (e - j) + 1; // 判断右边大的数有几个
	if (m > b)           // 若小于m个，在左边再取m-b个
		QuickSort(a, s, i, m - b);
	if (m < b)           // 若大于m个，在右边再去m个
		QuickSort(a, j + 1, e, m);
}
 
int main()
{
	int m,n;
	cin >> n >> m;
	for (int i = 0; i < n; i++)
		cin >> a[i];
	QuickSort(a, 0, n - 1, m);
	for (int i = n-1; i >= n-m; i--)
		cout << a[i] << ' ';
	cout << endl;
	system("pause");
}


