#include<iostream>
using namespace std;
int f(int m,int n){//m个苹果 n个盘子 
	if(n> m) //盘子多于苹果 
		return f(m,m);
	if(m == 0)//苹果数为0 
		return 1;
	if(n == 0)//盘子数为0 
		return 0;
	return f(m,n-1)+f(m-n,n);
}
int main(){
	int t,m,n;
	cin>>t;
	while(t--){
		cin>>m>>n;
		cout <<f(m,n)<<endl;
	}
	return 0;
}
