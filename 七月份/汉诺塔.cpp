#include<iostream>
using namespace std;
void Hanoi(int n, char src,char mid,char dest,int src_n)
//将src座上的n个盘子，以mid座为中转，移动到dest座
//src座上最上方盘子编号为src_n 
{
	if(n==1){//只需移动一个盘子 
		cout <<src_n<<":"<<src<<"->"<<dest<<endl;
		//直接将盘子从src移动到的desk即可 
		return ;//递归终止 
	}
	Hanoi(n-1,src,dest,mid,src_n);//先将n-1个盘子从src移动到mid 
	cout<<src_n + n - 1<<":"<<src<<"->"<<dest<<endl;
	//再将一个盘子从src移动到dest 
	Hanoi(n-1,mid,src,dest,src_n);//最后将n-1给盘子从mid移动到dest 
	return ; 
 } 

int main(){
	char a,b,c;
	int n;
	cin>>n>>a>>b>>c;//输入盘子数 
	Hanoi(n,a,b,c,1);
	return  0;
}
